from django.db import models


class Canvasser(models.Model):
	office_id = models.IntegerField(null=True)
	firstname = models.CharField(max_length=255, blank=False, null=False)
	lastname = models.CharField(max_length=255, blank=False, null=False)
	username = models.CharField(max_length=100, blank=False, null=False)
	external_id = models.IntegerField(null=True)

class CanvasserEvent(models.Model):
	canvasser_id = models.IntegerField()
	event_type = models.CharField(max_length=30,blank=True,null=False)
	event_time = models.DateTimeField()
	phone_time = models.DateTimeField(null=True)
	loc_accuracy = models.FloatField(null=True)
	loc_longitude = models.FloatField(null=True)
	loc_latitude = models.FloatField(null=True)
	battery_level = models.FloatField(null=True)
	battery_is_charging = models.NullBooleanField()
	activity_confidence = models.IntegerField(null=True)
	activity_type = models.CharField(max_length=30,blank=True,null=False)
	odometer = models.CharField(max_length=30,blank=True,null=False)
	loc_time = models.DateTimeField(null=True)
	loc_uuid = models.CharField(max_length=100,blank=True,null=False)
	loc_is_moving = models.NullBooleanField()
	loc_event = models.CharField(max_length=30,blank=True,null=False)
	site_id = models.IntegerField(null=True)
	sitecanvass_id = models.IntegerField(null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	ex_info = models.TextField(max_length=20, blank=True, null=True)
	packet_uuid = models.CharField(max_length=30,blank=True,null=True)
	doorcanvasspass_id = models.IntegerField(null=True)
	targetperson_id = models.IntegerField(null=True)
	turf_id = models.IntegerField(null=True)
	status = models.CharField(max_length=30,null=True)
	event_date_MST = models.DateField(null=True)

class CanvassResponse(models.Model):
	canvasserevent = models.ForeignKey(CanvasserEvent,null=False,on_delete=models.PROTECT)
	scriptelement_id = models.IntegerField()
	response_time = models.DateTimeField()
	response = models.CharField(max_length=500,blank=True,null=False)
	long_response = models.TextField(null=True)
	scan_time = models.IntegerField(null=True)
	created_at = models.DateTimeField(auto_now_add=True)


class CanvasserStatus(models.Model):
	canvasser_id = models.IntegerField()
	last_login = models.DateTimeField(null=True)
	last_checkin = models.DateTimeField(null=True)
	last_logresponse = models.DateTimeField(null=True)
	last_locationupdate = models.DateTimeField(null=True)
	last_knock = models.DateTimeField(null=True)
	latitude = models.FloatField(null=True)
	longitude = models.FloatField(null=True)
	battery_level = models.FloatField(null=True)
	accuracy = models.FloatField(null=True)
	is_logged_in = models.BooleanField(default=False)
	sitecanvass_id = models.IntegerField(null=True)
	is_checked_in = models.BooleanField(default=False)
	site_id = models.IntegerField(null=True)
	updated_at = models.DateTimeField(auto_now=True)
	bad_loc_on_last_update = models.NullBooleanField()



class CanvasserStatusLog(models.Model):
	log_datetime = models.DateTimeField(null=True)
	canvasser_id = models.IntegerField()
	last_login = models.DateTimeField(null=True)
	last_checkin = models.DateTimeField(null=True)
	last_logresponse = models.DateTimeField(null=True)
	last_locationupdate = models.DateTimeField(null=True)
	last_knock = models.DateTimeField(null=True)
	latitude = models.FloatField(null=True)
	longitude = models.FloatField(null=True)
	battery_level = models.FloatField(null=True)
	accuracy = models.FloatField(null=True)
	is_logged_in = models.BooleanField(default=False)
	sitecanvass_id = models.IntegerField(null=True)
	is_checked_in = models.BooleanField(default=False)
	site_id = models.IntegerField(null=True)
	bad_loc_on_last_update = models.NullBooleanField()


class CanvasserStatusLogArchive(models.Model):
	log_datetime = models.DateTimeField(null=True)
	canvasser_id = models.IntegerField()
	last_login = models.DateTimeField(null=True)
	last_checkin = models.DateTimeField(null=True)
	last_logresponse = models.DateTimeField(null=True)
	last_locationupdate = models.DateTimeField(null=True)
	last_knock = models.DateTimeField(null=True)
	latitude = models.FloatField(null=True)
	longitude = models.FloatField(null=True)
	battery_level = models.FloatField(null=True)
	accuracy = models.FloatField(null=True)
	is_logged_in = models.BooleanField(default=False)
	sitecanvass_id = models.IntegerField(null=True)
	is_checked_in = models.BooleanField(default=False)
	site_id = models.IntegerField(null=True)
	bad_loc_on_last_update = models.NullBooleanField()

class DeadLetterPacket(models.Model):
	req_body = models.TextField(null=True,blank=True)
	created_at = models.DateTimeField(auto_now_add=True)


class Campaign(models.Model):
	name = models.CharField(max_length=255,blank=False)
	description = models.CharField(max_length=255,blank=True,default='')
	campaign_type = models.CharField(max_length=20,blank=True,null=False)
	is_active = models.BooleanField(default=True)

class CampaignAggregate(models.Model):
	aggregation_string = models.TextField(null=True)
	campaign = models.ForeignKey(Campaign,null=True,on_delete=models.PROTECT)
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)


class MobilePacket(models.Model):
	req_body = models.TextField(null=True,blank=True)
	created_at = models.DateTimeField(auto_now_add=True)

class MobilePacketBackup(models.Model):
	req_body = models.TextField(null=True,blank=True)
	created_at = models.DateTimeField(auto_now_add=True)

class Site(models.Model):
	campaign = models.ForeignKey(Campaign,on_delete=models.PROTECT)
	name = models.CharField(max_length=255,blank=False)
	description = models.TextField(blank=True)
	addressline1 = models.CharField(max_length=255,blank=True)
	addressline2 = models.CharField(max_length=255,blank=True)
	city = models.CharField(max_length=100,blank=True)
	statecode = models.CharField(max_length=2,blank=True)
	zip5 = models.CharField(max_length=10,blank=True)
	lat = models.CharField(max_length=50,blank=True)
	lon = models.CharField(max_length=50,blank=True)
	site_type = models.CharField(max_length=50, blank=True, null=True)
	sig_per_hour_avg = models.FloatField(null=True)


# ALERTS MODELS
class Webhook(models.Model):
	endpoint = models.CharField(max_length=3000)
	headers = models.JSONField(default=dict)
	name = models.CharField(max_length=255)
	campaign = models.ForeignKey(Campaign, on_delete=models.PROTECT)
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'webhook_config'


class AlertConfig(models.Model):
	sitecanvass_id = models.IntegerField(null=True)
	doorcanvass_id = models.IntegerField(null=True)
	config = models.JSONField()
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'alert_config'


class AlertConfigWebhook(models.Model):
	alertconfig = models.ForeignKey(AlertConfig, on_delete=models.CASCADE, related_name='webhooks')
	webhook = models.ForeignKey(Webhook, on_delete=models.PROTECT, related_name='alertconfigs')

	class Meta:
		db_table = 'alert_config_webhook'


class DailyCanvasserSiteStats(models.Model):
	site = models.ForeignKey(Site, on_delete=models.PROTECT)
	canvasser = models.ForeignKey(Canvasser, on_delete=models.PROTECT)
	event_date_MST = models.DateField()
	stats = models.JSONField(default=dict)

