bq_query = """WITH  flattened_events as
(WITH events AS
    (SELECT 
        event_timestamp,
        event_name,
        event_params,
        user_pseudo_id
    FROM `woxapp-247619.analytics_229858064.events_DATESTRING` 
        where event_name in ('log_response', 'check_in', 'check_out', 'log_in', 'log_out'))
SELECT event_timestamp, event_name, user_pseudo_id, params
FROM events
CROSS JOIN UNNEST(events.event_params) params)
select params.value.string_value as packet_uuid,
        FROM flattened_events where params.key ='packet_uuid';"""

ce_query = """select
  packet_uuid
from
  main_canvasserevent
where
  date(event_time) = cast('DATESTRING' as date)
  and packet_uuid is not null;
"""

mp_query = """select json_extract(cast(req_body as json), '$.uuid') AS packet_uuid from main_mobilepacket 
where id > 34882716 
and date(created_at) = cast('DATESTRING' as date)
having packet_uuid is not null"""



## big query query

big_query = """SELECT 
    user_id,
    sum(case when event_name in('log_response', 'door_canvass', 'check_in', 'check_out', 'log_in', 'log_out') then 1 else 0 end) total_tracked_events_count,
    sum(case when event_name = 'log_response' then 1 else 0 end) log_response_count,
    sum(case when event_name = 'door_canvass' then 1 else 0 end) door_canvass_count,
    sum(case when event_name = 'check_in' then 1 else 0 end) check_in_count,
    sum(case when event_name = 'check_out' then 1 else 0 end) check_out_count,
    sum(case when event_name = 'log_in' then 1 else 0 end) log_in_count,
    sum(case when event_name = 'log_out' then 1 else 0 end) log_out_count
from
  (SELECT DISTINCT * FROM
  (WITH  flattened_events as
    (WITH events AS
        (SELECT 
            event_timestamp,
            event_name,
            event_params,
            user_pseudo_id
        FROM `woxapp-247619.analytics_229858064.events_DATESTRING` # this is where to change the table name
            where event_name in ('log_response', 'check_in', 'check_out', 'log_in', 'log_out'))
     SELECT event_timestamp, event_name, user_pseudo_id, params
      FROM events
     CROSS JOIN UNNEST(events.event_params) params)
   select 
         a.event_timestamp, 
         a.event_name, 
         a.user_pseudo_id, 
         b.phone_time, 
         CAST(c.user_id AS INT64) user_id,
         CAST(d.site_id AS INT64) site_id,
         CAST(e.shift_id AS INT64) shift_id,
      FROM flattened_events as a
      -- make phone_time a column
      LEFT JOIN (
               select event_timestamp, event_name, user_pseudo_id, params.value.string_value as phone_time,
              FROM flattened_events where params.key ='phone_time') as b
            on a.event_timestamp = b.event_timestamp
            and a.user_pseudo_id = b.user_pseudo_id
            and a.event_name = b.event_name
          -- make user_id a column
          LEFT JOIN (
               select event_timestamp, event_name, user_pseudo_id, params.value.double_value as user_id,
              FROM flattened_events where params.key ='wox_user_id') as c
            on a.event_timestamp = c.event_timestamp
            and a.user_pseudo_id = c.user_pseudo_id
            and a.event_name = c.event_name
        -- make site_id a column
        LEFT JOIN (
               select event_timestamp, event_name, user_pseudo_id, params.value.double_value as site_id,
              FROM flattened_events where params.key ='wox_site') as d
            on a.event_timestamp = d.event_timestamp
            and a.user_pseudo_id = d.user_pseudo_id
            and a.event_name = d.event_name
         -- make shift_id a column
        LEFT JOIN (
               select event_timestamp, event_name, user_pseudo_id, params.value.double_value as shift_id,
              FROM flattened_events where params.key ='wox_shift') as e
            on a.event_timestamp = e.event_timestamp
            and a.user_pseudo_id = e.user_pseudo_id
            and a.event_name = e.event_name)-- end of with flattened_events as call
  )
group by user_id
order by total_tracked_events_count desc;"""

sql_query = """select
    ce.canvasser_id,
    c.firstname,
    c.lastname,
    sum(case when ce.event_type in('log_response', 'door_canvass', 'check_in', 'check_out', 'log_in', 'log_out') then 1 else 0 end) total_tracked_events_count,
    sum(case when ce.event_type = 'log_response' then 1 else 0 end) log_response_count,
    sum(case when ce.event_type = 'door_canvass' then 1 else 0 end) door_canvass_count,
    sum(case when ce.event_type = 'check_in' then 1 else 0 end) check_in_count,
    sum(case when ce.event_type = 'check_out' then 1 else 0 end) check_out_count,
    sum(case when ce.event_type = 'log_in' then 1 else 0 end) log_in_count,
    sum(case when ce.event_type = 'log_out' then 1 else 0 end) log_out_count
from main_canvasserevent ce
         join main_canvasser c on c.id = ce.canvasser_id
where date(event_time) = cast('DATESTRING' as date)
group by ce.canvasser_id, c.firstname, c.lastname
order by total_tracked_events_count desc;"""
