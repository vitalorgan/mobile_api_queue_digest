"""
Django settings for q_digest project.

Generated by 'django-admin startproject' using Django 2.2.3.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deploymexnt/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'cb8b*z&ua4c=-c9ik-6kin%k#qo5me9vf^1==pvc9wc@x^4^4b'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
# DEBUG = True


ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'main',
    'scripts',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'q_digest.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'q_digest.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'wox_fw_prod',
        'USER': 'woxmaster',
        'PASSWORD': 'bHut56WewC78bcUrNN2Y41a9pQXsTvc',
        'HOST': 'woxprod-aurora-cluster.cluster-ctcfhrvoyzpg.us-east-2.rds.amazonaws.com',
        'PORT': '3306'
    },
}

DATABASES['read_only'] = {
    **DATABASES['default'],
    'HOST': 'woxprod-aurora-cluster.cluster-ro-ctcfhrvoyzpg.us-east-2.rds.amazonaws.com'
}



if DEBUG:
    DATABASES['default']['HOST'] = '127.0.0.1'
    DATABASES['default']['PORT'] = '3308'
    DATABASES['read_only']['HOST'] = '127.0.0.1'
    DATABASES['read_only']['PORT'] = '3309'


DATABASE_ROUTERS = ['q_digest.routers.MainRouter']

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'


### AWS SETTINGS ###

AWS_ACCESS_KEY_ID = 'AKIA42FVHGSNXESQ3MFZ'
AWS_SECRET_ACCESS_KEY = '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'
AWS_REGION_NAME = 'us-east-2'

AWS_SQS_URL = 'https://sqs.us-east-2.amazonaws.com/880848352411/wox-mobile-app-inbound'
DEAD_LETTER_QUEUE_URL = "https://sqs.us-east-2.amazonaws.com/880848352411/wox-mobile-app-deadletter"

ALERT_SQS_URL = 'https://sqs.us-east-2.amazonaws.com/880848352411/wox-alert'
ALERT_DEAD_LETTER_URL = 'https://sqs.us-east-2.amazonaws.com/880848352411/wox-alert-deadletter'


PUSH_NOTIFICATIONS_SQS_URL = 'https://sqs.us-east-2.amazonaws.com/880848352411/wox-app-push-notifications'
PUSH_NOTIFICATIONS_DEAD_LETTER_URL = 'https://sqs.us-east-2.amazonaws.com/880848352411/wox-app-push-notifications-deadletter'

#dev
# AWS_SQS_URL = 'https://sqs.us-east-2.amazonaws.com/880848352411/wox-mobile-app-dev'
# DEAD_LETTER_QUEUE_URL = "https://sqs.us-east-2.amazonaws.com/880848352411/wox-mobile-app-deadletter-dev"

# #

#python3 manage.py digest --queue https://sqs.us-east-2.amazonaws.com/880848352411/wox-mobile-app-deadletter

#purge
# AWS_SQS_URL = 'https://sqs.us-east-2.amazonaws.com/880848352411/wox-mobile-app-deadletter'
# DEAD_LETTER_QUEUE_URL = 'https://sqs.us-east-2.amazonaws.com/880848352411/purge'
