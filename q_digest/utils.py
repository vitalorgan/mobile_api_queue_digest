from math import radians, cos, sin, asin, sqrt


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (returns results in whatever units specified for r)
    """
    lon1, lat1, lon2, lat2 = map(radians, map(float, [lon1, lat1, lon2, lat2]))
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    r = 6371 * 1000  # Radius of earth in meters. Use 3956 for miles
    return c * r


def safeInt(val, default):
    try:
        return int(val)
    except:
        return default


def safeFloat(val, default):
    try:
        return float(val)
    except:
        return default