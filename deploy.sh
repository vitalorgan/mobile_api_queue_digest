#!/usr/bin/env bash



echo $"These are debug settings and target react env in order they appear:"
echo ""
grep 'DEBUG' ./q_digest/settings.py | sed -n '/^#/!p'| sed -n '/=/p'
grep 'AWS_SQS_URL' ./q_digest/settings.py | sed -n '/^#/!p'| sed -n '/=/p'
grep 'DEAD_LETTER_QUEUE_URL' ./q_digest/settings.py | sed -n '/^#/!p'| sed -n '/^\s*DEPLOYED/p' | sed -n '/=/p'
echo ""
echo 'branch:'
git branch | sed -n '/^\s*\*/p'

echo ""
echo "please press y if these settings are correct. Please make sure that your git branch and DEPLOYED_BRANCH setting match.
If you need to change anything, press any key to exit deployment"

while : ; do
read -n 1 k <&1
  if [[ $k = y ]] ; then
    break
  else
    echo $'\nYou exited deployment.'
    exit;
  fi
done

# to deploy to QUEUE DIGEST
scp -i ~/.ssh/portal.pem -r ./* ubuntu@3.21.146.227:/home/ubuntu/appq

#the queue is controlled by supervisor, so update conf files along with the new code
scp -i ~/.ssh/portal.pem -r ./* ubuntu@3.21.146.227:/home/ubuntu/appq
ssh -i ~/.ssh/portal.pem ubuntu@3.21.146.227 "sudo supervisorctl reread; sudo supervisorctl restart all;"

# to deploy to CRONSHOP INSTANCE
scp -i ~/.ssh/portal.pem -r ./supervisorconf/* ubuntu@3.132.164.41:/etc/supervisor/conf.d

# ##### ssh keys in case you need to ssh in and change anything
# queue digest
# ssh -i ~/.ssh/portal.pem ubuntu@3.21.146.227

# cronshop
# ssh -i ~/.ssh/portal.pem ubuntu@3.132.164.41



echo 'uploading zipped deployment to archive bucket'
DT=$(date +"%Y-%m-%dT%T")
zip -r ../last_digest_deployment.zip .
aws2 s3 mv ../last_digest_deployment.zip s3://queue-digest-deployment-archive/deployment_${DT}.zip


echo "SCP operations complete. Rebooting servers to reset supervisord."
#aws2 ec2 reboot-instances --instance-ids i-0fd0df28bf6f919af


count=1; while [ $count -le 12 ]; do
  echo 'pulling canvasser max id every 10 seconds to verify queue is being processed. Press CTL-C to exit. If you are getting errors,
  please ensure your ssh tunnel is enabled and pointing at port 3308'

  mysql \
    -h 127.0.0.1 \
    --port 3308 \
    -u woxmaster \
    -p'bHut56WewC78bcUrNN2Y41a9pQXsTvc' \
    -e 'select max(id) as MAX_EVENT_ID from wox_fw_prod.main_canvasserevent';
  ((count++))
  sleep 10
done





#dev deployment to portal instance for testing the dev queue

# scp -i ~/.ssh/portal.pem -r ./* ubuntu@3.132.164.41:/home/ubuntu/appq
