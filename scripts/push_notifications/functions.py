
import firebase_admin
from firebase_admin import credentials
import firebase_admin.messaging as messaging
from django.conf import settings
import os
import json
import ast
from scripts.digest.functions import send_message

cred = credentials.Certificate(os.path.join(settings.BASE_DIR, 'woxapp-247619-firebase-adminsdk-6h444-ca18cb08ae.json'))
default_app = firebase_admin.initialize_app(cred)

def dispatch_push_notification(config):
    notification = messaging.Notification(title=config.get('title', 'Wox Notification'), body=config.get('body'))
    TEST_TOKEN = 'fu3vX416SgSgOelvBe_rog:APA91bEsFTfDqM-g0hfvUPcsn5gPFI_BHHcqnGKaFkk49CnF7Hw33DoOJhvNPy7fqmHDezNwBEkvT9NYWygTeCsNIZ3HBR-v9ObSYjBu1hpsaQTeVd819eGrlpuamGVXIR-ttRF_0hdE'
    # This registration token comes from the client FCM SDKs.
    registration_token = config.get('fcm_token')

    # See documentation on defining a message payload.
    message = messaging.Message(
        data=config.get('data', {}),
        notification=notification,
        token=registration_token,
    )

    # Send a message to the device corresponding to the provided
    # registration token.
    response = messaging.send(message)
    # Response is a message ID string.
    print('Successfully sent message:', response)


def delete_message(client,queue_url,message):
    receipt_handle = message['ReceiptHandle']

    client.delete_message(
        QueueUrl=queue_url,
        ReceiptHandle=receipt_handle)

def receive_from_queue(client, queue_url, digest_count):

    response = client.receive_message(
        QueueUrl=queue_url,
        AttributeNames=[
            'SentTimestamp'
        ],
        MaxNumberOfMessages=digest_count,
        MessageAttributeNames=[
            'All'
        ])

    if 'Messages' in response:
        for m in response['Messages']:
            _m = m
            body = m['Body']
            print(body)

            try:
                j = json.loads(body)
            except:
                try:
                    m = ast.literal_eval(body)
                    j = json.loads(body)
                except BaseException as e:
                    print("LOAD ISSUE", e)
                    send_message(client, settings.PUSH_NOTIFICATIONS_DEAD_LETTER_URL, _m['Body'])
                    delete_message(client,queue_url, _m)
                    return

            try:
                dispatch_push_notification(j)
            except BaseException as e:
                print("failed to dispatch", e)
                send_message(client, settings.PUSH_NOTIFICATIONS_DEAD_LETTER_URL, _m['Body'])

            delete_message(client,queue_url, _m)
