from django.core.management.base import BaseCommand
from django.conf import settings
from google.cloud import bigquery
import pymysql
import csv
from q_digest import queries
import datetime


class Command(BaseCommand):
    help = 'S3 backup log dump'

    def add_arguments(queue, parser):
        parser.add_argument(
            '--year',
            help='The year that of that you want to check, defaults to current year in UTC Example: --year 2020',
        )

        parser.add_argument(
            '--month',
            help='the month that you want to check, defaults to current month in UTC. Example: --month 7',
        )

        parser.add_argument(
            '--day',
            help='the day of the month that you want to check, defaults to yesterday in UTC. Example: --day 12',
        )

        parser.add_argument(
            '--logs',
            help='Will return the log diff csv',
            action='store_true'
        )

        parser.add_argument(
            '--uuid',
            help="Will return the uuid diff csv",
            action='store_true'

        )

    def handle(self, *args, **options):
        client = bigquery.Client()
        db = settings.DATABASES
        conn = pymysql.connect(host=settings.DATABASES['default']['HOST'],
            user=db['default']['USER'],
            passwd=db['default']['PASSWORD'],
            db=db['default']['NAME'],
            port=int(db['default']['PORT']))

        now = datetime.datetime.now()
        year = options['year'] or '{:02d}'.format(now.year)
        month = options['month'] or '{:02d}'.format(now.month)
        day = options['day'] or '{:02d}'.format(now.day)

        sql_date_string = "{}-{}-{}".format(year, month, day)
        bq_date_string = "{}{}{}".format(year,month,day)

        if options['logs']:
            buildLogCsv(sql_date_string, bq_date_string, client, conn)
        if options['uuid']:
            buildUuidCsv(sql_date_string, bq_date_string, client, conn)

def buildLogCsv(sql_date_string, bq_date_string, client, conn):
        f = open("log.csv", 'w')

        writer = csv.writer(f)

        writer.writerow(["canvasser_id",
            "first_name",
            "last_name",
            "summed_events_ce",
            "summed_events_bq",
            "log_response_ce",
            "log_response_bq",
            "door_canvass_ce",
            "door-canvass_bq",
            "check_in_ce",
            "check_in_bq",
            "check_out_ce",
            "check_out_bq",
            "log_in_ce",
            "log_in_bq",
            "log_out_ce",
            "log_out_bq"])

        big_query_with_date = queries.big_query.replace("DATESTRING", bq_date_string)
        sql_query_with_date = queries.sql_query.replace("DATESTRING", sql_date_string)

        cursor = conn.cursor()
        cursor.execute(sql_query_with_date)
        data = cursor.fetchall()

        bigquery_job = client.query(big_query_with_date)

        bq_data = []
        bq_ids = []

        for row in bigquery_job:
            bq_data.append(row)
            bq_ids.append(row[0])

        for row in data:
            try:
                bq_id = bq_ids.index(row[0])
                print(bq_id)
            except:
                bq_id = False

            bq_row = [""] * 6 if bq_id == False else bq_data[bq_id]
            print(bq_row)
            writer.writerow([
                row[0],
                row[1],
                row[2],
                row[3],
                bq_row[1],
                row[4],
                bq_row[2],
                row[5],
                bq_row[3],
                row[6],
                bq_row[4],
                row[7],
                bq_row[5]])

        f.close()

def buildUuidCsv(sql_date_string, bq_date_string, client, conn):
    f = open('uuid.csv', 'w')
    writer = csv.writer(f)

    writer.writerow([
            'UUID',
            'IN_BQ',
            'IN_CE',
            'IN_MP'
    ])

    bq_job = client.query(queries.bq_query.replace("DATESTRING", bq_date_string))

    cursor = conn.cursor()
    cursor.execute(queries.ce_query.replace("DATESTRING", sql_date_string))
    ce_job = cursor.fetchall()

    cursor.execute(queries.mp_query.replace("DATESTRING", sql_date_string))
    mp_job = cursor.fetchall()

    uuid_dict = {}

    for row in bq_job:
        uuid_dict[row[0]] = [1, 0, 0]

    for row in ce_job:
        if row[0] in uuid_dict:
            uuid_dict[row[0]][1] = 1
        else:
            uuid_dict[row[0]] = [0, 1, 0]

    for row in mp_job:
        if row[0].replace('"', "") in uuid_dict:
            uuid_dict[row[0].replace('"', '')][2] = 1
        else:
            uuid_dict[row[0].replace('"', '')] = [0, 0, 1]

    correct_logs = 0
    for uuid in uuid_dict:
        if uuid_dict[uuid] == [1, 1, 1]:
            correct_logs += 1
            continue
        row = [uuid]
        row.extend(uuid_dict[uuid])
        writer.writerow(row)
    f.close()
