from django.core.management.base import BaseCommand
from django.conf import settings
from django.db.models import Q
from scripts.digest.functions import delete_message
import datetime
import boto3
from main.models import DeadLetterPacket
from django.conf import settings



"""
This dumps the api reject queue into a table for analysis.
"""

class Command(BaseCommand):

    def __init__(self):
        super(Command, self).__init__()
        self.client = boto3.client(
            'sqs',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
		    aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
		    region_name=settings.AWS_REGION_NAME
        )
        self.QUEUE_URL = 'https://sqs.us-east-2.amazonaws.com/880848352411/wox-mobile-app-api-rejects'

    def add_arguments(self, parser):
        parser.add_argument(
            '--count',
            help='How many messages to pull. If omitted pulls the whole queue',
        )

    def get_num_messages(self):
        response = self.client.get_queue_attributes(
            QueueUrl= self.QUEUE_URL,
            AttributeNames=[
                'ApproximateNumberOfMessages'
            ],
        )
        try:
            num = response['Attributes']['ApproximateNumberOfMessages']
            print(num)
        except KeyError:
            print("ApproximateNumberOfMessages key was not found in the response dictionary.")
            return 0
        return num




    def pull_from_queue(self, pull_count):

        i = 0
        failures = 0
        pull_count = int(pull_count)

        while i < pull_count:

            response = self.client.receive_message(
                QueueUrl=self.QUEUE_URL,
                AttributeNames=[
                    'SentTimestamp'
                ],
                MessageAttributeNames=[
                    'All'
                ],
                MaxNumberOfMessages =10,
                VisibilityTimeout=0,
                WaitTimeSeconds=0)

            messages_in_response = 1
            if 'Messages' in response:
                messages_in_response = len(response['Messages'])
                for m in response['Messages']:
                    try:
                        print(m['Body'])
                        DeadLetterPacket(req_body=m['Body']).save()
                    except:
                        print("Unable to parse and save packet! Failures: %d so far" % failures)
                        failures +=1
                        break

                    delete_message(self.client, self.QUEUE_URL, m)



            i += messages_in_response
        print("completed with %d failures out of %d packets" % (failures, pull_count))


    def handle(self, *args, **options):
        if options['count']:
            pull_count = options['count']
        else:
            pull_count = self.get_num_messages()

        self.pull_from_queue(pull_count)


