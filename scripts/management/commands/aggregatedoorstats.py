# coding=utf-8

from django.core.management.base import BaseCommand
from django.db import connection
from scripts.digest.functions import *
import datetime
from main.models import CanvasserEvent, CampaignAggregate, Campaign
import urllib
import json


now = datetime.datetime.now()

class Command(BaseCommand):

    help = 'Aggregate key metrics for each turf and update turf table'

    def dictfetchall(self, cursor):
        """Return all rows from a cursor as a dict"""
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

    def add_arguments(queue, parser):

        parser.add_argument(
            '--turfs',
            help='Update counts for turfs',
            action='store_true'
        )

        parser.add_argument(
            '--doorcanvasspasses',
            help="Update counts for door canvass passes",
            action='store_true'

        )


    def run_aggregation_query(self, *args, **options):

        #TODO : ADD A WHERE CLAUSE TO ONLY UPDATE RECENTLY UPDATED TURFS

        #build a json array of attempted and contacted doors for each turf (updates the agg_data column_
        turfcall = """
            call aggregates__turfs_walkcount();
        """

        #add last contact time and status for recently contacted targets
        targetcall = """
        update targetperson tp
            join (
                         select targetperson_id, event_time, status
                         from main_canvasserevent
                         where id in (select max(id)
                                      from main_canvasserevent
                                      where targetperson_id is not null
                                        and event_type = 'door_canvass'
                                        and event_date_MST  >= SUBDATE(CURDATE(), INTERVAL 1 DAY)
                                      group by targetperson_id)
            ) aggs on tp.id = aggs.targetperson_id
            set tp.last_attempt_status = aggs.status,
            tp.last_attempt_time = aggs.event_time;
        """
        #doorcanvass_pass_call -- same thing as turfs, but updates agg_data column for door canvasses
        dc_pass_call = """
            call aggregates__update_dcp_stats();
        """

        with connection.cursor() as cursor:
            if options['turfs']:
                print("executing turf update")
                cursor.execute(turfcall)
            if options['doorcanvasspasses']:
                print("executing dcp update")
                cursor.execute(dc_pass_call)
            cursor.execute(targetcall)
            return True





    def handle(self, *args, **options):
        start = datetime.datetime.now()
        res = self.run_aggregation_query(*args, **options)
        print("query completed. It took: ", datetime.datetime.now() - start)








'''


put here for reference: reset houshold count in doorcanvasspass table

update doors_doorcanvasspass canvasspass
    join (
        select count(distinct household_id) household_count,
               doorcanvasspass_id
        from targetperson
        group by doorcanvasspass_id
    ) aggs on canvasspass.id = aggs.doorcanvasspass_id
set
    canvasspass.household_count = aggs.household_count;

'''

