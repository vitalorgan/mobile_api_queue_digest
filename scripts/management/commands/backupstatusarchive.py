from django.core.management.base import BaseCommand
from django.conf import settings
from scripts.CustomQuery import CustomQuery
import csv
import boto3
import os
import datetime
from botocore.exceptions import ClientError

class Command(BaseCommand):

    def today_string(self):
        return datetime.date.today().strftime("%Y-%m-%d")

    def get_s3_client(self):
        return boto3.client(
            's3',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
            region_name=settings.AWS_REGION_NAME
        )


    def upload_file(self, file_name, bucket, object_name=None):
        # If S3 object_name was not specified, use file_name
        if object_name is None:
            object_name = file_name

        # Upload the file
        s3_client = self.get_s3_client()
        try:
            response = s3_client.upload_file(file_name, bucket, object_name)
        except ClientError as e:
            ###need to do something if it fails!!!
            print(e)
            return False
        return True


    def handle(self, *args, **options):


        field_array = [
            'log_datetime',
            'last_login',
            'last_checkin',
            'last_logresponse',
            'last_locationupdate',
            'latitude',
            'longitude',
            'battery_level',
            'accuracy',
            'is_logged_in',
            'is_checked_in',
            'canvasser_id',
            'site_id',
            'sitecanvass_id'
        ]


        query_field_string = CustomQuery.make_select_string(field_array)

        #how many days should we keep in the table?
        DAYS_TO_KEEP = 7

        query = """
                select  %s from main_canvasserstatuslogarchive
                where date(log_datetime) <= subdate(curdate(), %d)
        """ % (query_field_string, DAYS_TO_KEEP)


        sql = CustomQuery()
        results = sql.make_query(query)

        print(len(results))


        tempfilename = 'writefile.csv'
        with open(tempfilename, 'w') as out:
            csv_out = csv.writer(out)
            csv_out.writerow(field_array)
            for row in results:
                csv_out.writerow(row)



        s3_filename="%s-canvasser-status-archive-backup.csv" % self.today_string()

        with open(tempfilename, 'r+') as f:
            success = self.upload_file(tempfilename, "wox-daily-data-store", s3_filename)
            if success:
                f.truncate()

                deletion_query = """
                    delete from main_canvasserstatuslogarchive 
                    where date(log_datetime) <= SUBDATE(curdate(), %d);
                """ % (DAYS_TO_KEEP)


                sql.make_query(deletion_query)
            else:
                os.rename(tempfilename, '/home/ubuntu/failed_backups/' + s3_filename)

