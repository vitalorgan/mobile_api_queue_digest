from django.core.management.base import BaseCommand
from django.conf import settings
from django.db.models import Q
from scripts.digest.functions import *
import datetime
import boto3
from main.models import CanvasserStatus, CanvasserStatusLog


now = datetime.datetime.now()

class Command(BaseCommand):

    def handle(self, *args, **options):
        now = datetime.datetime.now()
        threshold = now - datetime.timedelta(minutes = 5)
        allCanvassers = list(CanvasserStatus.objects
                             .filter(updated_at__gte=threshold)
                                .values()
                             )


        for c_status in allCanvassers:
            print(c_status)
            del c_status["id"]
            del c_status["updated_at"]
            c_status["log_datetime"] = now
            CanvasserStatusLog.objects.create(**c_status)


