from django.core.management.base import BaseCommand
from django.conf import settings
from scripts.push_notifications.functions import *
import time
import boto3

class Command(BaseCommand):
    help = 'SQS digest loop'

    def add_arguments(queue, parser):
        parser.add_argument(
            '--wait',
            help='Wait between messages',
        )

        parser.add_argument(
            '--count',
            help='Number of messages to process in each cycle',
        )

        parser.add_argument(
            '--queue',
            help='Which queue to send to ',
        )

    def handle(self, *args, **options):

        sqs = boto3.client(
            'sqs',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
            region_name=settings.AWS_REGION_NAME)

        if options['wait']:
            wait_time = int(options['wait']) / 1000
        else:
            ## in ms, to be clear
            wait_time = 500 / 1000

        if options['count']:
            digest_count = int(options['count'])
        else:
            ## in ms, to be clear
            digest_count = 1

        if options.get('queue'):
            queue_url = options.get('queue')
        else:
            queue_url = settings.PUSH_NOTIFICATIONS_SQS_URL


        print("QUEUE: ", queue_url)
        while True:
            receive_from_queue(
                client=sqs,
                queue_url=queue_url,
                digest_count=digest_count)
            time.sleep(wait_time)










