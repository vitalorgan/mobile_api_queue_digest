# coding=utf-8

from django.core.management.base import BaseCommand
from django.db import connection
from scripts.digest.functions import *
import datetime
from main.models import CanvasserEvent, CampaignAggregate, Campaign
import urllib
import json


now = datetime.datetime.now()

class Command(BaseCommand):

    help = 'Aggregate how many doors in each canvass have been attempted'

    def dictfetchall(self, cursor):
        """Return all rows from a cursor as a dict"""
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]


    def run_aggregation_query(self):


        #TODO : ADD A WHERE CLAUSE TO ONLY UPDATE RECENTLY UPDATED TURFS
        call = """
        update doors_doorcanvasspass canvasspass
            join (
                select JSON_OBJECT('attempted', count(distinct targetperson_id),
                                           'canvassed', sum(if(status = 'canvass', 1, 0))) agg_data,
                               canvasspass_id
                        from (
                                 select cp.id canvasspass_id, ce.targetperson_id, ce.status, ce.event_time
                                 from main_canvasserevent ce
                                          join doors_doorcanvasspass cp
                                               on ce.doorcanvasspass_id = cp.id
                                 where ce.id in (select max(id)
                                                 from main_canvasserevent
                                                 where targetperson_id is not null
                                                   and event_type = 'door_canvass'
                                                    and event_date_MST  >= SUBDATE(CURDATE(), INTERVAL 3 DAY)
                                                 group by targetperson_id)
                             ) a
                        group by canvasspass_id
                    ) aggs on canvasspass.id = aggs.canvasspass_id
                set
                    canvasspass.agg_data = aggs.agg_data;
        """

        with connection.cursor() as cursor:
            cursor.execute(call)
            return True



    def handle(self, *args, **options):
        res = self.run_aggregation_query()










