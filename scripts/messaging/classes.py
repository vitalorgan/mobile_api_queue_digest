import boto3
from q_digest.settings import AWS_SECRET_ACCESS_KEY, AWS_ACCESS_KEY_ID, ALERT_SQS_URL, ALERT_DEAD_LETTER_URL
import requests
import json
from django.db import connection
from main.models import *


# this is the class that we use for dispatching
class SQSAlertDispatcher:
    """Class We use for building alerts, and dispatching them to the queue """

    def __init__(self, alert = {}):
        self.client = boto3.client(
            "sqs",
            aws_access_key_id=AWS_ACCESS_KEY_ID,
            aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
            region_name="us-east-2"
        )

        self.alert = alert

    def dispatch(self, deadletter=False):
        if deadletter:
            sqs_url = ALERT_DEAD_LETTER_URL
        else:
            sqs_url = ALERT_SQS_URL

        try:
            self.client.send_message(
                QueueUrl=sqs_url,
                MessageBody=json.dumps(self.alert)
            )
        except BaseException as e:
            print("ERROR DISPATCHING ALERT MESSAGE", e)

        return self

    def delete_from_queue(self, receipt_handle):
        self.client.delete_message(
            QueueUrl=ALERT_SQS_URL,
            ReceiptHandle=receipt_handle)



class AlertProcessor:

    def __init__(self, alert_obj):
        self.alert = alert_obj
        self.send_points = []

    def pull_expanded_data(self):
        query = """
            select a.id          alert_id,
                   a.value       value,
                   a.threshold   threshold,
                   atype.name    alert_type,
                   atype.unit_name unit_name,
                   ce.event_time event_time,
                   sc.id         sitecanvass_id,
                   sc.name       sitecanvass_name,
                   c.id          canvasser_id,
                   c.firstname   firstname,
                   c.lastname    lastname,
                   s.name        site_name
            from main_alert a
                     join main_canvasserevent ce
                          on a.canvasserevent_id = ce.id
                     join main_sitecanvass sc
                          on ce.sitecanvass_id = sc.id
                     join main_canvasser c
                          on ce.canvasser_id = c.id
                     join main_site s
                          on ce.site_id = s.id
                     join main_alerttype atype
                          on a.alert_type_id = atype.id
                        where a.id = %s;
        """ % self.alert['id']

        with connection.cursor() as cursor:
            cursor.execute(query)
            return dictfetchall(cursor)[0]


    def get_send_points(self, sitecanvass_id=None):
        if not sitecanvass_id:
            sitecanvass_id = CanvasserEvent.objects.get(id=self.alert['canvasserevent_id']).sitecanvass_id
        alert_type_id = self.alert['alert_type_id']
        self.send_points = list(AlertChannel.objects\
                .filter(alert_type_id=alert_type_id, sitecanvass_id=sitecanvass_id)\
                .values('alert_type', 'contact_value', 'config', 'channel_type__name'))
        return self.send_points



    def push_to_db(self):
        print(self.alert)
        al = Alert(
            alert_type_id=self.alert['alert_type_id'],
            canvasserevent_id=self.alert['canvasserevent_id'],
            canvasser_id=self.alert['canvasser_id'],
            threshold=self.alert['threshold'],
            value=self.alert['value']
        )
        al.save()
        self.alert['id'] = al.id
        print(al)



    def send_alerts(self):
        self.get_send_points()
        print("SEND POINTS", self.get_send_points())
        data = self.pull_expanded_data()


        for point in self.send_points:
            _type = point['channel_type__name']
            if _type == 'sms':
                print("firing sms")
                sendstring = """
                Wox Alert: %s %s triggered a %s alert at site %s. Threshold was %s %s, value was %s.
                """ % (data['firstname'], data['lastname'], data['alert_type'], data['site_name'],
                       round(data['threshold']), data['unit_name'], round(data['value']))

                SnsSms().send(point['contact_value'], sendstring)

            elif _type == 'email':
                print("firing email")
                pass

            elif _type == 'webhook':
                print("firing webhook")
                serializable_data = {**data, 'event_time': data['event_time'].strftime('%Y-%m-%d %H:%M:%S')}
                r = requests.post('http://requestbin.net/r/13z1st31', data=json.dumps(serializable_data))



