import boto3
import sys
import ast
import json
import time
from datetime import datetime, timedelta
from main.models import *
from scripts.messaging.classes import *
from q_digest.settings import ALERT_DEAD_LETTER_URL
import requests

#max alert frequency for one canvasser/type in minutes
MAX_CANV_TYPE_FREQ = 20


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]



def receive_from_queue(client,queue_url,digest_count):

    response = client.receive_message(
        QueueUrl=queue_url,
        AttributeNames=[
            'SentTimestamp'
        ],
        MaxNumberOfMessages=digest_count,
        MessageAttributeNames=[
            'All'
        ])

    if 'Messages' in response:
        for alert_message in response['Messages']:
            dispatcher = SQSAlertDispatcher(alert_message['Body'])
            try:
                alert = json.loads(alert_message['Body'])
            except:
                try:
                    alert_message = ast.literal_eval(alert_message['Body'])
                    alert = json.loads(alert_message['Body'])
                except BaseException as e:
                    print("[ALERT] Alert was sent to the deadletter queue. An error was thrown when trying to load json from the Body key at queue ingestion.", e, '\nFull Queue Message:\n', _m, '\n')
                    alert = {'event': ''}
                    dispatcher.dispatch(True) #True flag for deadletter
                    return


            #throttle -- get last call here to ensure not less than freq
            try:
                for webhook in alert['config']['webhooks']:
                    # need to sanitize headers so that all values are strings
                    headers = {k: str(v) for (k, v) in webhook['headers'].items()}
                    # print("CHECK CHECK", alert, headers)
                    r = requests.post(webhook['endpoint'], json=alert, headers=headers, allow_redirects=False)
                    print(r)
            except Exception as e:
                print("[ALERT] Alert was sent to the deadletter queue. An error was thrown when trying to load json from the Body key at queue ingestion.", e, '\nFull Queue Message:\n', _m, '\n')
                alert = {'event': ''}
                dispatcher.dispatch(True)

            dispatcher.delete_from_queue(alert_message['ReceiptHandle'])





