import json
from django.utils import timezone
from django.forms.models import model_to_dict
from django.db import connection
from datetime import datetime, timedelta, date
from dateutil import parser
from q_digest.utils import haversine
from django.core.exceptions import ObjectDoesNotExist
from main.models import *
from scripts.messaging.classes import SQSAlertDispatcher
from scripts.messaging.functions import dictfetchall


def cbool(f):
	try:
		if f == True:
			ret_val = True
		elif f == False:
			ret_val = False
		else:
			ret_val = None
	except:
		ret_val = None

	return ret_val


class Event(object):
	def __init__(self,message):
		try:
			tstamp = message['MessageAttributes']['EventTime']['StringValue']
			# tstamp = message['Attributes']['SentTimestamp']
		except KeyError as e:
			print("[Alert] MessageAttributes key not found. Stripping time from Attributes key instead.")
			tstamp = message['Attributes']['SentTimestamp']
			tstamp = float(tstamp)/1000

		time_fmt = '%Y-%m-%d %H:%M:%S'
		date_fmt = '%Y-%m-%d'

		try:
			body = json.loads(message['Body'])
		except:
			body = {}

		try:
			self.canvasser_id = body['user']['id']
		except:
			try:
				self.canvasser_id = body['canvasser_id']
			except:
				self.canvasser_id = 1

		try:
			self.event_type = body['event']
		except:
			self.event_type = ''

		try:
			self.event_time = datetime.fromtimestamp(float(tstamp)).strftime(time_fmt)
		except:
			self.event_time = None

		try:
			self.event_date_MST = (datetime.strptime(body['phone_time'], '%Y-%m-%dT%H:%M:%SZ') - timedelta(hours=7)).strftime(date_fmt)
		except:
			# self.event_date_MST = None
			try:
				self.event_date_MST = (datetime.fromtimestamp(float(tstamp)) - timedelta(hours=7)).strftime(date_fmt)
			except:
				self.event_date_MST = None

		try:
			self.doorcanvasspass_id = body['doorcanvasspass_id']
		except:
			try:
				self.doorcanvasspass_id = body['doorcanvass_pass_id']
			except:
				self.doorcanvasspass_id = None

		try:
			self.targetperson_id = body['target_id']
		except:
			self.targetperson_id = None

		try:
			self.status = body['status']
		except:
			self.status = None

		try:
			self.turf_id = body['turf_id']
		except:
			self.turf_id = None

		try:
			self.phone_time = datetime.strptime(body['phone_time'], '%Y-%m-%dT%H:%M:%SZ').strftime(time_fmt)
		except:
			self.phone_time = None

		try:
			self.loc_accuracy = body['location']['coords']['accuracy']
		except:
			self.loc_accuracy = None

		try:
			self.loc_longitude = body['location']['coords']['longitude']
		except:
			self.loc_longitude = None

		try:
			self.loc_latitude = body['location']['coords']['latitude']
		except:
			self.loc_latitude = None

		try:
			self.battery_level = body['location']['battery']['level']
		except:
			self.battery_level = None

		try:
			self.battery_is_charging = cbool(body['location']['battery']['is_charging'])
		except:
			self.battery_is_charging = None

		try:
			self.activity_confidence = body['location']['activity']['confidence']
		except:
			self.activity_confidence = None

		try:
			self.activity_type = body['location']['activity']['type']
		except:
			self.activity_type = ''

		try:
			self.odometer = body['location']['odometer']
		except:
			self.odometer = ''

		try:
			self.loc_time = parser.parse(body['location']['timestamp']).strftime(time_fmt)
		except:
			self.loc_time = None

		try:
			self.loc_uuid = body['location']['uuid']
		except:
			self.loc_uuid = ''

		try:
			self.loc_is_moving = cbool(body['location']['is_moving'])
		except:
			self.loc_is_moving = None

		try:
			self.loc_event = body['location']['event']
		except:
			self.loc_event = ''

		self.site_id = body.get('site', None)
		self.sitecanvass_id = body.get('sitecanvass', None)

		self.packet_uuid = body.get('uuid', None)

		try:
			self.ex_info = json.dumps(body.get('phone_data'))
			if self.ex_info == 'null':
				self.ex_info = None
		except BaseException as e:
			print("JSON ex_info error: ", e)
			self.ex_info = ''

class ResultSet(object):
	def __init__(self,message):

		time_fmt = '%Y-%m-%d %H:%M:%S'
		rs = []

		try:
			body = json.loads(message['Body'])
		except:
			body = {}

		try:
			for m in body['response']['responses']:
				val = m['value']
				if len(val) > 500:
					long_val = val
					val = None
				else:
					long_val = None
				res = {
					'scriptelement_id': m['element_id'],
					'response': val,
					'long_response': long_val,
					'scan_time': m.get('scanTime'),
					'response_time': parser.parse(m['time']).strftime(time_fmt)
				}

				rs.append(res)
		except:
			print("RESULT SET ERROR. RESPONSES NOT SAVED.")

		self.responses = rs




class Status(Event):

	def getStatusObject(self):
		try:
			status = CanvasserStatus.objects.get(canvasser_id = self.canvasser_id)
		except ObjectDoesNotExist:
			print("Canvasser Status not found. Making a new one.")
			status = CanvasserStatus(canvasser_id=self.canvasser_id)
		return status

	def updateStatus(self):
		status = self.getStatusObject()
		status.accuracy = self.loc_accuracy
		status.sitecanvass_id = self.sitecanvass_id
		status.battery_level = self.battery_level

		if self.loc_longitude and self.loc_latitude:
			status.longitude = self.loc_longitude
			status.latitude = self.loc_latitude
			status.bad_loc_on_last_update = False
		else:
			status.bad_loc_on_last_update = True

		if self.event_type in ("check_in", "check_out", "location_update", "log_response", "create_site"):
			status.is_logged_in = True

		if self.event_type == 'log_in':
			status.last_login = self.phone_time

		elif self.event_type == 'log_out':
			status.is_logged_in = False
			status.site_id = None

		elif self.event_type == 'check_in':
			status.is_checked_in = True
			status.last_checkin = self.phone_time
			status.site_id = self.site_id

		elif self.event_type == 'check_out':
			status.is_checked_in = False
			status.site_id = None

		elif self.event_type == 'location_update':
			status.last_locationupdate = self.loc_time

		elif self.event_type == 'log_response':
			status.last_logresponse = self.phone_time

		elif self.event_type == 'create_site':
			pass

		elif self.event_type == 'door_canvass':
			status.last_knock = self.phone_time

		status.save()







''' 

	EACH EVENT TYPE NEEDS A CHECK
	
	
	 ARCHITECTURE OF THE CHECK
	 
	 1) PULL ALERTS FOR ASSOCIATED SITE CANVASS
	 2) PARSE THE ALERTS BASED ON THE FOLLOWING:
	 		- IS THE EVENT_TYPE MATCH THE KINDS OF ALERTS YOU ARE LOOKING FOR?
	 		- IF SO, WHAT OTHER INFORMATION DO WE NEED?
	 			-- EVENT COUNT? 
	 			-- APPLICABLE DOOR OR SITE FOR DISTANCE
	 			-- RESPONSE 
	 			
	3) I NEED TO UPDATE THE MOBILE APP AND ADD DOORCANVASS_ID TO ALL PACKETS

'''

DOOR_CANVASS = 'DOOR_CANVASS'
SITE_CANVASS = 'SITE_CANVASS'


class AlertCheck:


	def __init__(self, queue_message):
		# self.checkeable_events_location = ['log_response', 'check_in']
		# self.checkeable_events_time = ['log_response']
		self.distance_threshold = None # in meters
		self.time_threshold = None # in minutes
		self.event_time_format = '%Y-%m-%d %H:%M:%S'
		self.message = queue_message
		self.event = Event(queue_message).__dict__ # where event is the result of __dict__ method of events class with canvasserevent_id added
		self.dispatcher = SQSAlertDispatcher()
		self.ALERT_TYPES = ['distance_from_target', 'event_is', 'event_count', 'response_is']
		self.EVENT_TYPE = SITE_CANVASS if self.event.get('sitecanvass_id', False) else DOOR_CANVASS


	def compare_with_text_operator(self, value, threshold, operator):

		try:
			if operator == '<':
				return float(value) < float(threshold)
			if operator == '>':
				return float(value) > float(threshold)
			if operator == '=' or operator == 'is_exactly':
				return value == threshold
			if operator == 'contains':
				return str(value) in str(threshold)
			if operator == 'starts_with':
				return str(threshold).startswith(str(value))
		except Exception as e:
			print("[ERROR] comparison error", value, threshold, operator, e)
			return False


	def check_event_is(self, config, canvasser):
		if self.event['event_type'] in config.get('event_types', []):
			dispatcher = SQSAlertDispatcher({
				"event": self.event,
				"config": config,
				"canvasser": canvasser,
				"description": f"{canvasser['firstname']} {canvasser['lastname']} submitted an event of type {self.event['event_type']}"
			})
			dispatcher.dispatch()

	def check_distance_from_target(self, config, canvasser):

		if self.event['event_type'] in config.get('event_types', []):
			try:
				value_threshold = int(config['value_threshold'])
			except:
				print("[ERROR] no value threshold in distance check", self.event)
				return

			canvasser_point = {"lat": self.event['loc_latitude'], "lon": self.event['loc_longitude']}
			if self.EVENT_TYPE == DOOR_CANVASS and self.event.get('targetperson_id') and self.event.get('doorcanvasspass_id'):
				try:
					query = f'''
						select household.* from
							targetperson person
							join targethousehold household 
								on person.household_id = household.id
							where person.id = {self.event['targetperson_id']}	
							and person.doorcanvasspass_id = {self.event.doorcanvasspass_id}
					'''
					with connection.cursor() as cursor:
						cursor.execute(query)
						resp = dictfetchall(cursor)
						cursor.close()

					target_point = resp[0]
					target_point['lat'] = target_point['latitude']
					target_point['lon'] = target_point['longitude']

				except Exception as e:
					print(f"[ERROR]: Failed to pull door to compute distance alarm.", e, self.event)

			elif self.EVENT_TYPE == SITE_CANVASS:
				try:
					target_point = {"type": "site", **model_to_dict(Site.objects.get(id=self.event['site_id']))}

				except Exception as e:
					print(f"[ERROR]: Failed to pull site to compute distance alarm.", e, self.event)

			else:
				alarm_triggered = False
			try:
				distance = haversine(canvasser_point["lon"], canvasser_point["lat"], target_point["lon"], target_point["lat"])
				alarm_triggered = self.compare_with_text_operator(distance, config["value_threshold"], config["operator"])

				if alarm_triggered:
					dispatcher = SQSAlertDispatcher({
						"event": self.event,
						"target_point": target_point,
						"distance": distance,
						"config": config,
						"canvasser": canvasser,
						"description": f"{canvasser['firstname']} {canvasser['lastname']} submitted a {self.event['event_type']} event that was {int(distance)} meters from their {target_point['type']}"
					})
					dispatcher.dispatch()

			except:
				print("[ERROR] could not calculate distance alarm")



	def check_event_count_in_time_period(self, config, canvasser):
		if self.event['event_type'] in config.get('event_types', []):
			try:
				value_threshold = int(config['value_threshold'])
				# a large number could stall the queue
				if value_threshold > 70:
					value_threshold = 70
				time_threshold = int(config['time_threshold'])
			except:
				print("[ERROR] value or time threshold error in event check", self.event)
				return

			try:
				events = (CanvasserEvent.objects
					.filter(canvasser_id=self.event['canvasser_id'], event_type__in=config.get('event_types',[]))
					.order_by('-id'))[:value_threshold]
				time_cutoff = datetime.now() - timedelta(seconds=(time_threshold + 1) * 60) #add one minute to prevent faulty alarms
				events = [x for x in events if x.phone_time > time_cutoff]

				alarm_triggered = self.compare_with_text_operator(len(events), value_threshold, config["operator"])


				if alarm_triggered:
					dispatcher = SQSAlertDispatcher({
						"event": self.event,
						"config": config,
						"canvasser": canvasser,
						"description": f"{canvasser['firstname']} {canvasser['lastname']} submitted {len(events)} events"
									   f" of type {config.get('event_types')} in {time_threshold} minutes. "
									   f"Your cutoff was {config['operator']} than {value_threshold}"
					})
					dispatcher.dispatch()

			except Exception as e:
				print("[ERROR] could not calculate event_count alarm", self.event, e)


	def check_response_is(self, config, canvasser):

		if self.event['event_type'] != 'log_response' or not config['script_element'] or not config['operator']:
			return

		#get the target response
		responses = ResultSet(self.message).responses
		response = None
		for r in responses:
			if r['scriptelement_id'] == config['script_element']:
				response = r

			print("found response", response)

		#didn't find it
		if not response:
			return

		try:
			operator = config['operator']
			alarm_triggered = False

			answer = response['response'] or response['long_response']

			if operator == 'exists':
				alarm_triggered = bool(answer)

			if operator == 'is_exactly':
				alarm_triggered = answer.lower() == config['value_threshold'].lower()

			if operator == 'starts_with':
				alarm_triggered = (answer.lower()).startswith(config['value_threshold'].lower())

			if operator == 'contains':
				alarm_triggered = config['value_threshold'].lower() in answer.lower()

			if alarm_triggered:

				print('firing alarm for question')

				dispatcher = SQSAlertDispatcher({
					"event": self.event,
					"response": answer,
					"config": config,
					"canvasser": canvasser,
					"description": f"The response to {config.get('script_element_name')} was {answer}"
				})
				dispatcher.dispatch()

		except Exception as e:
			print("[ERROR] failed to parse response check alert.", self.event, responses, config, e)




	def pull_canvass_configs(self):
		if self.EVENT_TYPE == SITE_CANVASS:
			# TODO: CHANGE SITE CANVASS ID;
			configs = AlertConfig.objects.filter(sitecanvass_id=self.event['sitecanvass_id'])
		elif self.EVENT_TYPE == DOOR_CANVASS and self.event.get('doorcanvasspass_id'):
			try:
				doorcanvass_id_query = '''
					SELECT canvass_id from wox_fw_prod.doors_doorcanvasspass where id = %d
				''' % self.event['doorcanvasspass_id']

				with connection.cursor() as cursor:
					cursor.execute(doorcanvass_id_query)
					resp = dictfetchall(cursor)
					cursor.close()
					doorcanvass_id = resp[0]['canvass_id']

				configs = AlertConfig.objects.filter(doorcanvass_id=doorcanvass_id)
			except Exception as e:
				print("[ERROR] could not parse the doorcanvass id", self.event, e)
		else:
			return []

		configs = configs.prefetch_related('webhooks', 'webhooks__webhook')
		return [{
			"id": config.id,
			"webhooks": [model_to_dict(webhook.webhook) for webhook in config.webhooks.all()],
			 **config.config
		} for config in configs]


	def pull_canvasser_info(self):
		canvasser_id = self.event['canvasser_id']
		try:
			canvasser = Canvasser.objects.get(id=canvasser_id)
		except:
			print(f"[ERROR]: Unable to pull canvasser with id {canvasser_id}")
			canvasser = Canvasser.objects.get(id=1)

		return model_to_dict(canvasser)


	def check(self):

		configs = self.pull_canvass_configs()
		canvasser = self.pull_canvasser_info()

		# print("configs", configs)

		for config in configs:
			trigger = config.get('trigger')

			if trigger == 'event_is':
				self.check_event_is(config, canvasser)

			if trigger == 'distance_from_target':
				self.check_distance_from_target(config, canvasser)

			if trigger == 'event_count':
				self.check_event_count_in_time_period(config, canvasser)

			if trigger == 'response_is':
				self.check_response_is(config, canvasser)









