import boto3
import sys
import ast
import json
import time
from datetime import datetime, timedelta, date
from main.models import *
from scripts.digest.classes import Event, Status, ResultSet, AlertCheck
from q_digest.settings import DEAD_LETTER_QUEUE_URL
from django.db import connection

def extract_date(message):
	t = message['MessageAttributes']['EventTime']['StringValue']
	return datetime.fromtimestamp(float(t))

def extract_receipt(message):
	return message['ReceiptHandle']

def delete_message(client,queue_url,message):
	receipt_handle = message['ReceiptHandle']

	client.delete_message(
	QueueUrl=queue_url,
	ReceiptHandle=receipt_handle)

def send_message(client, queue_url, message):
	client.send_message(
	QueueUrl=queue_url,
	MessageBody=message)





#sql to update door canvass status in realtme
def update_door_target_status(targetperson_id, status):

	try:
		targetperson_id = int(targetperson_id)
	except:
		print("[ERROR] Value Error with target id while updating door target status.")
		return

	query = '''
		update targetperson
		set last_attempt_status = '%s',
			last_attempt_time = CURRENT_TIME()
		where id = %d;
	''' % (status, int(targetperson_id))

	with connection.cursor() as cursor:
		cursor.execute(query)



def event__save(client,queue_url,message):

	event = Event(message).__dict__
	status = Status(message)

	try:
		#create both canvasser status and canvasser event.
		canvasser_event = CanvasserEvent.objects.create(**event)
		event['canvasserevent_id'] = canvasser_event.id
		status.updateStatus()

	except Exception as event:
		#send message to dead_letter queue
		print("[ALERT] Message was sent to the deadletter queue. An error was thrown when trying to create the CanvasserEvent object.", event, '\n', message, '\n')
		send_message(client, DEAD_LETTER_QUEUE_URL, message['Body'])
		return

	try:
		if event['event_type'] == 'log_response':
			response_set = ResultSet(message).responses
			for resp in response_set:
				r = CanvassResponse.objects.create(canvasserevent=canvasser_event, **resp)

	except Exception as e:
		print("[ALERT] The responses failed to save.", e, '\nFull Queue Message:\n', message)


	try:
		if event.get('status') and event.get('targetperson_id') and event['event_type'] == 'door_canvass':
			update_door_target_status(event['targetperson_id'], event['status'])
	except BaseException as e:
		print("[ERROR] Failure to update target status for: ", event, e)

	return event
	# try:
	# if event['event_type'] in ['log_response', 'check_in']:
	# 	check = AlertCheck(event)
	# 	check.check()
	# except Exception as e:
	# 	print("[ALERT] Failed to run AlertCheck.", e, '\nEvent:\n', event)




def process_event_for_alerts(message):
	alert_check = AlertCheck(message)
	alert_check.check()

def receive_from_queue(client, queue_url, digest_count):

	response = client.receive_message(
		QueueUrl=queue_url,
		AttributeNames=[
			'SentTimestamp'
		],
		MaxNumberOfMessages=digest_count,
		MessageAttributeNames=[
			'All'
		])

	if 'Messages' in response:
		for m in response['Messages']:
			_m = m
			# print(m)
			try:
				j = json.loads(m['Body'])
			except:
				try:
					m = ast.literal_eval(m['Body'])
					j = json.loads(m['Body'])
				except BaseException as e:
					print("[ALERT] Message was sent to the deadletter queue. An error was thrown when trying to load json from the Body key at queue ingestion.", e, '\nFull Queue Message:\n', _m, '\n')
					j = {'event': '', 'user': {}}
					send_message(client, DEAD_LETTER_QUEUE_URL, m['Body'])

			if 'event' in j:
				if j['event'] in ('log_in',
								  'log_out',
								  'check_in',
								  'check_out',
								  'location_update',
								  'log_response',
								  'create_site',
								  'door_canvass',
								  'site_contact_attempt',
								  ):
					if j['user'].get('full_name', '') == '__TRAINER__':
						print("TRAINING PACKET, DELETING")
						delete_message(client,queue_url, _m)
					else:
						# print("SAVING: \n", _m)
						event = event__save(client, queue_url, _m)
						process_event_for_alerts(_m)

				else:
					print("BAD EVENT, SENDING TO DEADLETTER", m)
					send_message(client, DEAD_LETTER_QUEUE_URL, m['Body'])
			else:
				print("NO EVENT, SENDING TO DEADLETTER", m)
				send_message(client, DEAD_LETTER_QUEUE_URL, m['Body'])

			delete_message(client, queue_url, _m)







