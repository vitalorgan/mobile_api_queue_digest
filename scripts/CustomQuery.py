
from django.db import connection

class CustomQuery:

    def make_query(self, query):
        cursor = connection.cursor()
        cursor.execute(query)
        results = cursor.fetchall()

        return results

    @staticmethod
    def make_select_string(field_array):
        res = ''
        for idx, field in enumerate(field_array):
            if idx != len(field_array)-1:
                res = res + field + ', '
            else:
                res = res + field

        return res
